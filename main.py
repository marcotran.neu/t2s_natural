from fastapi import FastAPI,Body
from ForwardTacotron.notebook_utils.synthesize import Synthesizer
import soundfile as sf
from typing import Dict, Any, List
import uuid
import base64
import os

app = FastAPI()

async def generate_audio_encode(input_text: str):
    output_audio_file = "/tmp/" + str(uuid.uuid1()) + ".wav"
    synth_forward = Synthesizer(tts_path='forward_step90k.pt')
    synth_fastpitch = Synthesizer(tts_path='fastpitch_step200k.pt')
    wav = synth_forward(input_text, voc_model='melgan', alpha=1)
    sf.write(output_audio_file, wav, synth_fastpitch.dsp.sample_rate)
    encode_string = base64.b64encode(open(output_audio_file, "rb").read())
    os.remove(output_audio_file)
    return encode_string


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/hello/{name}")
async def say_hello(name: str):
    return {"message": f"Hello {name}"}

@app.post("/generate_speech")
async def generate_speech(body: Dict[str, Any] = Body(...)):
    input_text = body["input_text"]
    encode_audio = generate_audio_encode(input_text)
    return {"encode_audio": encode_audio}

