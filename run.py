from ForwardTacotron.notebook_utils.synthesize import Synthesizer
import soundfile as sf
synth_forward = Synthesizer(tts_path='forward_step90k.pt')
synth_fastpitch = Synthesizer(tts_path='fastpitch_step200k.pt')


input_text = '''Hey guys, I recently spent some time in Hong Kong and let me tell you, it's amazing! '''
# wav = synth_forward(input_text, voc_model='griffinlim', alpha=1)
wav = synth_forward(input_text, voc_model='griffinlim')

sf.write('new_file.wav', wav, synth_fastpitch.dsp.sample_rate)